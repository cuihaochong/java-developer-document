### 开发环境

阿里云盘链接：https://www.aliyundrive.com/s/JZAFEwfUUC1

> tips: 阿里云盘分享目前存在bug，分享的部分文件可能无权限查看和下载

![soft](images/java-soft.png)

### Other Tools

[mysql、redis、mq等可通过`docker-compose`直接安装，点击进入查看详情](https://gitee.com/zhengqingya/docker-compose)

---

> 每天学习一点点，慢慢日积月累，你总会成为你喜欢的样子，加油。
